#include<avr/io.h>
#include<util/delay.h>
#include<stdlib.h>
#include "lcd.h"

void adc_init(){
	ADMUX = (1<<REFS0);						//Reference Selection
	ADCSRA = (1<<ADPS1);						//Division Factor 4>>250 kHz
	ADCSRA |= (1<<ADEN); 						//Enabling ADC
}

int adc_read(){
	ADMUX |= 0;									//Channel selection AC0
	ADCSRA |= (1<<ADSC);						//Start Convertion
	int value = ADCL;							//Store Value 
	value |= (ADCH<<8);					
	return(value);
}


void main()
{	
	char a[3];
	adc_init();
	LCD_Init();
	while(1){
        int b=adc_read()/2; 
		itoa(b,a,10);
		LCD_Clear();
        LCD_DisplayString("Temperature: ");
		LCD_DisplayString(a);
		_delay_ms(300); 
      }
}
