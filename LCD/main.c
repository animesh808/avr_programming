#include <avr/io.h>
#include <util/delay.h>
#include<lcd.h>
#define RS 6
#define E  5
void send_a_command (unsigned char command);
void send_a_character(unsigned char character);
int main(void)
{
    DDRA = 0xFF;
    DDRD = 0xFF;
    _delay_ms(50);
    send_a_command(0x01);// sending all clear command
    send_a_command(0x38);// 16*2 line LCD
    send_a_command(0x0E);// screen and cursor ON
    
    
LCDWriteString_sliding(200,"TASFIA TASBIN");
    
}
void send_a_command (unsigned char command)
{
    PORTA=command;
    PORTD&= ~(1<<RS);
    PORTD|= (1<<E);
    _delay_ms(50);
    PORTD&= ~(1<<E);
    PORTA =0;
}
