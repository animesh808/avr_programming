//#define F_CPU 1000000UL
#include<avr/io.h>
#include<util/delay.h>

void timer_init()
{
	TCCR0 |= (1<<CS02)|(1<<CS00); //set prescaler
	
}


void main()
{
	DDRC |=1<<PINC0;
	timer_init();
	while(1)
	{
		if(TCNT0>=96) //check for number in timer register
		{
			PORTC ^=1<<PINC0;
			TCNT0 = 0;
		}
	}
}