#include<avr/io.h>
#include<util/delay.h>

void adc_init(){
	ADMUX = (1<<REFS0);						//Reference Selection
	ADCSRA = (1<<ADPS2);						//Division Factor 4>>250 kHz
	ADCSRA |= (1<<ADEN); 						//Enabling ADC
}

int adc_read(){
	ADMUX |= 0;									//Channel selection AC0
	ADCSRA |= (1<<ADSC);						//Start Convertion
	int value = ADCL;							//Store Value 
	value |= (ADCH<<8);
	while(ADCSRA&0b01000000){
	}						
	return(value);
}

void display(int num){
	int digit[4]={0,0,0,0};
	int i=0;
	int count=4;
    while (num != 0) {
		digit[i] = num % 10;
		num = num / 10;
		i++;
	}
	
	for(int j=3;j>=0;j--){
		PORTC=(digit[j]|0b11110000); 
        PORTC&=~(1<<count);   
        _delay_ms(1);
		count++;
	}
}

void main()
{
    DDRC=0b11111111;
	adc_init();
  
	while(1){
        int b=adc_read(); 
        display(b);   
      }
}
