#include <avr/io.h>
 
#define F_CPU	4000000UL
#include <util/delay.h>
 
int main(void)
{
	DDRD = 0xFF;
	
	int digits[15] = {0x3F, 0x06,0x5B,0x4F,0x66,0x6D,0x7D,0x07,0x7F,0x67};
 
    while(1)
    {
		for(int i=0;i<10;i++){
			PORTD = digits[i];	
			_delay_ms(100);	
		}
		
    }
 
	return 0;
}
