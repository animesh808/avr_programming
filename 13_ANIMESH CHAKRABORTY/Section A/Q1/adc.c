#include<avr\io.h>
#include<util\delay.h>
#include "adc.h"

void adc_init(){
	ADMUX = (1<<REFS0);						
	ADCSRA = (1<<ADPS2);					
	ADCSRA |= (1<<ADEN); 
}						

int adc_read(){
	ADMUX |= (1<<MUX0)|(1<<MUX1);									
	ADCSRA |= (1<<ADSC);						
	int value = ADCL;							 
	value |= (ADCH<<8);
	while(ADCSRA&0b01000000){
	}						
	return(value);
}