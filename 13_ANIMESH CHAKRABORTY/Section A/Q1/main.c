#include<avr\io.h>
#include<util\delay.h>
#include "adc.h"
#include "7seg.h"
#define ldr_thresh 600

int main()
{
	DDRC=0b11111111;
	DDRD=0b00000100;
	adc_init();
	int adc_value,count=0;
	while(1)
	{
		adc_value=adc_read();
		if(adc_value>ldr_thresh)
		{
			PORTD |= 1<<PIND2;
			count++;
		}
		else
		{
			PORTD &= 0<<PIND2;
		}
		display(adc_value);
	}
}