#include<avr\io.h>
#include<avr\interrupt.h>
#include<util\delay.h>

int count=0;
int digits[15] = {0x3F, 0x06,0x5B,0x4F,0x66,0x6D,0x7D,0x07,0x7F,0x67};
void interrupt_init()
{
	SREG|=(1<<7);
	MCUCR|=(1<<ISC11)|(1<<ISC10);
	GICR|=(1<<INT1);
}


int main()
{
	DDRC = 0xff;
	interrupt_init();
	while(1){
	}
}

ISR(INT1_vect)
{
	count++;
	PORTC = digits[count];
	if(count>9) count = 0;
}