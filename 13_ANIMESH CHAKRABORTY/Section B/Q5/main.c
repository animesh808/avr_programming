#include<avr/io.h>
#include<util/delay.h>

int main(){

	DDRB |=0b01010101;
	int flag=0;
	while(1){
		PORTB |= 1<<flag;
		_delay_ms(100);
		PORTB &= 0<<flag;
		flag=flag+2;
		if(flag>6) flag=0;
	}
}