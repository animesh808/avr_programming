#define F_CPU 8000000UL
#include <avr/io.h>
#include <util/delay.h>
#include "adc.h"


int main(void)
{
	adc_init();
	DDRB |= 0b00000100;
	while (1)
	{
		int sensor_reading = adc_read();
		if(sensor_reading > 200){
			PORTB |= 1<<PB2;
		}
		else{
			PORTB &= ~(1<<PB2);
		}
	}
}