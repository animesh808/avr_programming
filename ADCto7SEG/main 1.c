#define F_CPU 8000000UL

#include <avr/io.h>

#include <util/delay.h>

//void Sev_Seg (char Number);


/*void Sev_Seg (char Number) // Function to write the BCD of a number to the seven segment pins (PC4-7)

{

  switch (Number)

  {

    case 0:

      {
        PORTC &= ~(1 << 4) & ~(1 << 5) & ~(1 << 6) & ~(1 << 7);
        //PORTC &= 0b00001111;

        break;

      }

    case 1:

      {
        PORTC |= (1 << 4);

        PORTC &= ~(1 << 5) & ~(1 << 6) & ~(1 << 7);

        break;

      }

    case 2:

      {
        PORTC |= (1 << 5);

        PORTC &= ~(1 << 4) & ~(1 << 6) & ~(1 << 7);

        break;

      }

    case 3:

      {
        PORTC |= (1 << 4) | (1 << 5);

        PORTC &= ~(1 << 6) & ~(1 << 7);

        break;

      }

    case 4:

      {
        PORTC |= (1 << 6);

        PORTC &= ~(1 << 4) & ~(1 << 5) & ~(1 << 7);

        break;

      }

    case 5:

      {
        PORTC |= (1 << 4) | (1 << 6);

        PORTC &= ~(1 << 5) & ~(1 << 7);

        break;

      }

    case 6:

      {

        PORTC |= (1 << 5) | (1 << 6);

        PORTC &= ~(1 << 4) & ~(1 << 7);

        break;

      }

    case 7:

      {

        PORTC |= (1 << 4) | (1 << 5) | (1 << 6);

        PORTC &= ~(1 << 7);

        break;

      }

    case 8:

      {

        PORTC |= (1 << 7);

        PORTC &= ~(1 << 4) & ~(1 << 5) & ~(1 << 6);

        break;

      }

    case 9:

      {

        PORTC |= (1 << 7) | (1 << 4);

        PORTC &= ~(1 << 5) & ~(1 << 6);

        break;

      }
  }
}*/

void Display(int num);

void Display(int num)
{
	PORTC = num;

}

int main(void)

{

  char count = 0; // This is the variable that will be displayed on the seven-segment

  char i = 0;

  DDRC |= 0b11111111; // Set direction for port B as output

  PORTC &= 0b00000000; // Set the all the seven segment pins to 0 in the beginning
  //third_seg = 0;
  //fourth_seg = 2;

  int val = 1024;
  int digit[4];
  int k = 0;
  while (val != 0) {
    digit[k] = val % 10;
    val = val / 10;
    k++;
  }
 


  while (1)

  {

    //for (count=1;count<=45;count+=2) // This loop is to increment the number that will be displayed

    //{


    for (i = 0; i <= 10; i++) // This loop introduces some more delay so that the counting is slow enough for our eyes to recognize it

    {

      //Sev_Seg(0); //write the third digit of "count" value to the first seven-segment
      Display(digit[2]);

      PORTC &= ~(1 << 1); //disable the first seven-segment
      _delay_ms(2);
      PORTC |= (1 << 1); //Enable the third seven-segment



      //Sev_Seg(2); //write the fourth digit of "count" value to the second seven-segment
      Display(digit[3]);

      PORTC &= ~(1 << 0); //disable the fourth seven-segment
      _delay_ms(2);
      PORTC |= (1 << 0); //Enable the fourth seven-segment


      //Sev_Seg(count%10); //write the first digit of "count" value to the first seven-segment
      Display(digit[0]);

      PORTC &= ~(1 << 3); //disable the second seven-segment
      _delay_ms(2);
      PORTC |= (1 << 3); //Enable the second seven-segment




      //Sev_Seg(count/10); //write the second digit of "count" value to the second seven-segment
      Display(digit[1]);

      PORTC &= ~(1 << 2); //disable the first seven-segment
      _delay_ms(2);
      PORTC |= (1 << 2); //Enable the first seven-segment


    }
    //}
  }
}
