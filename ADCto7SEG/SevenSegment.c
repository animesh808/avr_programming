#include<avr/io.h>
#include<util/delay.h>


void display(int num){
    int digit[4];
    int i=0;
    int count=4;
	while (num != 0) {
   	 digit[i] = num % 10;
   	 num = num / 10;
   	 i++;
    }
    
    for(int j=3;j>=0;j--){
   	 PORTA=(digit[j]|0b11110000);
    	PORTA&=~(1<<count);   
    	_delay_ms(1);
   	 count++;
    }
}
