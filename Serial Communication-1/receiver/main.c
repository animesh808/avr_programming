#include <avr/io.h>
#include <util/delay.h>
#include <stdlib.h>
#include "lcd.h"                            
  
#define BAUD 9600                           
#define BAUDRATE ((F_CPU)/(BAUD*16UL)-1)   
  
#ifndef F_CPU
#define F_CPU 16000000UL                    
#endif
  
void uart_init (void)
{
    UBRRH=(BAUDRATE>>8);
    UBRRL=BAUDRATE;                         
    UCSRB|=(1<<TXEN)|(1<<RXEN);             
    UCSRC|=(1<<URSEL)|(1<<UCSZ0)|(1<<UCSZ1);
}
  

void uart_transmit (unsigned char data)
{
    while (!( UCSRA & (1<<UDRE)));            
    UDR = data;                            
}

unsigned char uart_recieve (void)
{
    while(!(UCSRA) & (1<<RXC));          
    return UDR;                            
}
 
int main (void){

    unsigned char a[5];
    char buffer[10];
	int i=0;
    uart_init();                            
    LCD_Init();           					                         
	LCD_Clear();
  
    while(1)
    {
        a[i]=uart_recieve();                   
		i++;
		if(i>5) break;
        _delay_ms(100);                   
    }	
	LCD_DisplayString(a);				
}