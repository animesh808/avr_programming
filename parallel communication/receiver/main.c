#include<avr/io.h>
#include<stdio.h>
#include<util/delay.h>
#include<stdlib.h>
#include "lcd.h"


int main(){
	DDRB = 0;
	DDRC = 0xff;
	DDRD = 0xff;
	LCD_Init();
	
	int j=0;
	while(1)
	{
		char msg[4]={0,0,0,0,0};
		LCD_Clear();
		for(int i=0;i<5;i++){
			PORTC = PINB;
			char a = PORTC;
			msg[i]=a;
			_delay_ms(200);
		}
		LCD_DisplayString(msg);
		break;
	}
}