#define LCD_PORT1        PORTD         /**< port1 for the LCD lines  */
#define LCD_PORT2        PORTB         /**< port2 for the LCD lines  */
#define LCD_DATA0_PORT   LCD_PORT2     /**< port for 4bit data bit 0 */
#define LCD_DATA1_PORT   LCD_PORT2     /**< port for 4bit data bit 1 */
#define LCD_DATA2_PORT   LCD_PORT2     /**< port for 4bit data bit 2 */
#define LCD_DATA3_PORT   LCD_PORT2     /**< port for 4bit data bit 3 */
#define LCD_DATA0_PIN    0             /**< pin for 4bit data bit 0  */
#define LCD_DATA1_PIN    1             /**< pin for 4bit data bit 1  */
#define LCD_DATA2_PIN    2             /**< pin for 4bit data bit 2  */
#define LCD_DATA3_PIN    3             /**< pin for 4bit data bit 3  */
#define LCD_RS_PORT      LCD_PORT1     /**< port for RS line         */
#define LCD_RS_PIN       5             /**< pin  for RS line         */
#define LCD_RW_PORT      LCD_PORT1     /**< port for RW line         */
#define LCD_RW_PIN       6             /**< pin  for RW line         */
#define LCD_E_PORT       LCD_PORT1     /**< port for Enable line     */
#define LCD_E_PIN        7             /**< pin  for Enable line     */

void LCD_Init();
void LCD_Clear();
void LCD_GoToLineOne();
void LCD_GoToLineTwo();
void LCD_GoToXY(char row, char col);
void LCD_CmdWrite( char cmd);
void LCD_DataWrite( char dat);
void LCD_DisplayString(char *string_ptr);
void LCD_DisplayNumber(unsigned int num);
void LCD_ScrollMessage(char *msg_ptr);
void LCD_DisplayRtcTime(char hour,char min,char sec);
void LCD_DisplayRtcDate(char day,char month,char year);
