#include<avr\io.h>
#include<util\delay.h>
#include "adc.h"
#include "7seg.h"
#define ldr_thresh 512

int main()
{
	DDRC=0b11111111;
	DDRB=0b00000001;
	adc_init();
	int adc_value,count=0;
	while(1)
	{
		adc_value=adc_read();
		if(adc_value<ldr_thresh)
		{
			PORTB |= 1<<PINB0;
			count++;
		}
		else
		{
			PORTB &= 0<<PINB0;
		}
		display(adc_value);
	}
}