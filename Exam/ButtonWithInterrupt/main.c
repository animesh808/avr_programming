#include<avr/io.h>
#include<avr/interrupt.h>
#include<util/delay.h>

int count=0;
int digits[15] = {0x3F, 0x06,0x5B,0x4F,0x66,0x6D,0x7D,0x07,0x7F,0x67};
void interrupt_init()
{
	SREG|=(1<<7);
	MCUCR|=(1<<ISC01)|(1<<ISC00);
	GICR|=(1<<INT0);
}


int main()
{
	DDRA = 1<<PIN0;
	DDRC = 0xff;
	interrupt_init();
	while(1){
	}
}

ISR(INT0_vect)
{
	count++;
	PORTA ^=1<<PIN0;
	PORTC = digits[count];
	if(count>9) count = 0;
}