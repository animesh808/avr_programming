#include<avr/io.h>
#include<util/delay.h>

int main()
{
	DDRB = 0xff;           // Configure PORTC as output to connect Leds
    DDRA = 0x00;           // Configure PORTA as INput to connect switches
	DDRD = 0xff;
    PORTA = 0xff; 
	//PORTD = 0xff;// Enable The PullUps of PORTA.
	int count=0;
	int digits[15] = {0x3F, 0x06,0x5B,0x4F,0x66,0x6D,0x7D,0x07,0x7F,0x67};
    while(1)
    {
        PORTB = PINA;     // Read the switch status and display it on Leds
		if(PINB == 0b11111110) 
		{
			count++;
			PORTD=digits[count];
			_delay_ms(100);
			if(count>9) count = 0;
		}
    }
}