#include<avr\io.h>
#include<util\delay.h>
#include "adc.h"

void adc_init(){
	ADMUX = (1<<REFS0);						//Reference Selection
	ADCSRA = (1<<ADPS2);						//Division Factor 4>>250 kHz
	ADCSRA |= (1<<ADEN); 						//Enabling ADC
}

int adc_read(){
	ADMUX |= 0;									//Channel selection AC0
	ADCSRA |= (1<<ADSC);						//Start Convertion
	int value = ADCL;							//Store Value 
	value |= (ADCH<<8);
	while(ADCSRA&0b01000000){
	}						
	return(value);
}