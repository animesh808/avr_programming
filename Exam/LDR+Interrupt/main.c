#include<avr/io.h>
#include<avr/interrupt.h>
#include<util/delay.h>
#include "adc.h"
#include "7seg.h"

#define ldr_thresh 512


int digits[15] = {0x3F, 0x06,0x5B,0x4F,0x66,0x6D,0x7D,0x07,0x7F,0x67};
int adc_value;

void interrupt_init()
{
	SREG|=(1<<7);
	MCUCR|=(1<<ISC01)|(1<<ISC00);
	GICR|=(1<<INT0);
}

int main()
{
	DDRC = 0xff;
	DDRA = 1<<PINA0;
	
	interrupt_init();
	adc_init();
	
	while(1)
	{
	}
}

ISR(INT0_vect)
{
	adc_value=adc_read();
	
	if(adc_value>ldr_thresh)
	{
		PORTA |=1<<PINA0;
	}
	else
	{
		PORTA &=0<<PINA0;
	}
	display(adc_value);
}