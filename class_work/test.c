#include<avr/io.h>

int main()
{
	DDRB = DDRB | 0b11111111;
	DDRA = DDRA | 0b11111110;
	
	while(1){
		PORTB = PORTB^0b00000001;
		char button_status = PINA & 0b00000001;
		
		if(button_status)
			PINB = PINB^0b00101011;
		else
			PINB = PINB^0b01010101;
	}
}